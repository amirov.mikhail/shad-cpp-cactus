#include <cactus/test.h>

#include <cactus/cactus.h>

#include "socks4.h"

struct FakeConn : public cactus::IConn {
    size_t Read(cactus::MutableView buf) override {
        size_t n = cactus::Copy(buf, cactus::View(in));
        in.erase(0, n);
        return n;
    }

    void Write(cactus::ConstView buf) override {
        out += std::string(reinterpret_cast<const char*>(buf.data()), buf.size());
    }

    void Close() override {}

    const folly::SocketAddress& RemoteAddress() const override {
        throw std::runtime_error("not implemented");
    }

    const folly::SocketAddress& LocalAddress() const override {
        throw std::runtime_error("not implemented");
    }

    std::string in;
    std::string out;
};

FIBER_TEST_CASE("Protocol Check") {
    FakeConn c;

    c.in = {0x00, 0x5a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    Sock4Connect(&c, folly::SocketAddress("1.2.3.4", 80), "prime");

    REQUIRE(c.out.size() == 8 + 6);
    REQUIRE(c.out.substr(2, 2) == std::string("\x00\x50", 2));
    REQUIRE(c.out.substr(4, 4) == std::string("\x01\x02\x03\x04"));
    REQUIRE(c.out.substr(8) == std::string("prime\0", 6));
}

FIBER_TEST_CASE("Error Check") {
    FakeConn c;

    c.in = {0x00, 0x11};
    c.in += std::string(6, '\0');

    REQUIRE_THROWS(Sock4Connect(&c, folly::SocketAddress("1.2.3.4", 80), "prime"));
}

FIBER_TEST_CASE("Size Check") {
    FakeConn c;

    REQUIRE_THROWS(Sock4Connect(&c, folly::SocketAddress("1.2.3.4", 80), "prime"));
}

FIBER_TEST_CASE("Direct connect") {
    auto lsn = cactus::ListenTCP(folly::SocketAddress("127.0.0.1", 0));

    auto conn = DialProxyChain({}, lsn->Address());

    auto serverConn = lsn->Accept();

    REQUIRE(conn->RemoteAddress() == lsn->Address());
    REQUIRE(conn->LocalAddress() == serverConn->RemoteAddress());
}

FIBER_TEST_CASE("Chain connect") {
    auto lsn = cactus::ListenTCP(folly::SocketAddress("127.0.0.1", 0));

    std::vector<Proxy> proxies = {
        {lsn->Address(), "prime"},
        {folly::SocketAddress("8.8.8.8", 22), "levvv"},
    };

    cactus::Fiber f([&] {
        std::string goodReply = {0x00, 0x5a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        auto conn = lsn->Accept();

        std::string req(8+6, '\0');
        conn->ReadFull(cactus::View(req));

        REQUIRE(req.substr(2, 2) == std::string("\x00\x16", 2));
        REQUIRE(req.substr(4, 4) == std::string("\x08\x08\x08\x08"));
        REQUIRE(req.substr(8) == std::string("prime\0", 6));

        conn->Write(cactus::View(goodReply));

        conn->ReadFull(cactus::View(req));

        REQUIRE(req.substr(4, 4) == std::string("\x01\x02\x03\x04"));
        REQUIRE(req.substr(8) == std::string("levvv\0", 6));

        conn->Write(cactus::View(goodReply));

        std::string pong = "pong";
        conn->Write(cactus::View(pong));
    });

    auto conn = DialProxyChain(proxies, folly::SocketAddress("1.2.3.4", 1234));
    REQUIRE(conn->ReadAllToString() == "pong");
}
