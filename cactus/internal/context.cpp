#include "context.h"

#include "cactus/core/debug.h"

#include <cstdint>

namespace __cxxabiv1 {                          // NOLINT
typedef void __untyped_cxa_exception;           // NOLINT
struct __cxa_eh_globals {                       // NOLINT
    __untyped_cxa_exception* CaughtExceptions;  // NOLINT
    unsigned int UncaughtExceptions;            // NOLINT
};
extern "C" __cxa_eh_globals* __cxa_get_globals() noexcept;  // NOLINT
}  // namespace __cxxabiv1

extern "C" cactus::MachineContext* CactusContextActivate(cactus::MachineContext* ctx,
                                                         cactus::SwitchArgs* data);
extern "C" void CactusContextTrampoline();
extern "C" void CactusContextEntry(cactus::MachineContext* saved_ctx) noexcept;

namespace cactus {

struct SwitchArgs {
    SavedContext* from = nullptr;
    SavedContext* to = nullptr;
};

SavedContext::SavedContext() = default;

SavedContext::SavedContext(char* stack, size_t stack_size) {
    auto stack_top = stack + stack_size;
    DCHECK(reinterpret_cast<int64_t>(stack_top) % 16 == 0);

    // Push extra qword, that way rsp+8 aligns on 16 bytes after trampoline.
    *reinterpret_cast<uint64_t*>(stack_top - 8) = 0;

    // Create fake frame on the stack.
    rsp = reinterpret_cast<MachineContext*>(stack_top - sizeof(MachineContext) - 8);
    *rsp = {};

    // First activation works as if fiber was suspended just before calling CactusContextTrampoline.
    rsp->saved_rip = reinterpret_cast<void*>(&CactusContextTrampoline);
}

// Cache pointer in TLS for faster access.
thread_local __cxxabiv1::__cxa_eh_globals* eh_globals = __cxxabiv1::__cxa_get_globals();

static_assert(sizeof(__cxxabiv1::__cxa_eh_globals) == SavedContext::kErasedSize);

void SavedContext::Activate(SavedContext* suspend_to) {
    *reinterpret_cast<__cxxabiv1::__cxa_eh_globals*>(eh.data()) = *eh_globals;
    *eh_globals = {};

    SwitchArgs args;
    args.from = suspend_to;
    args.to = this;
    DCHECK(suspend_to != this);

    auto ctx = rsp;
    rsp = nullptr;
    DCHECK(ctx);

    auto save_point = CactusContextActivate(ctx, &args);
    auto switch_args = reinterpret_cast<SwitchArgs*>(save_point->data);
    switch_args->from->rsp = save_point;
    DCHECK(switch_args->to == suspend_to);

    *eh_globals = *reinterpret_cast<__cxxabiv1::__cxa_eh_globals*>(eh.data());
    eh = {};
}

}  // namespace cactus

extern "C" void CactusContextEntry(cactus::MachineContext* parent) noexcept {
    auto args = reinterpret_cast<cactus::SwitchArgs*>(parent->data);
    args->from->rsp = parent;

    auto entry = args->to->entry;
    args->to->entry = nullptr;
    DCHECK(entry);

    // Should never return.
    entry->Run();
    std::terminate();
}