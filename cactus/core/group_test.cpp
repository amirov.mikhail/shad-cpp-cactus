#include <cactus/test.h>

#include <cactus/cactus.h>

using namespace cactus;

FIBER_TEST_CASE("Spawn fibers in group") {
    int done = 0;

    ServerGroup g;

    g.Spawn([&] {
        SleepFor(std::chrono::milliseconds(1));
        done++;
    });

    g.Spawn([&] {
        SleepFor(std::chrono::milliseconds(1));
        done++;
    });

    SleepFor(std::chrono::milliseconds(10));
    REQUIRE(done == 2);
}

FIBER_TEST_CASE("Server group cancel") {
    int done = 0;

    {
        ServerGroup g;
        g.Spawn([&] {
            try {
                SleepFor(std::chrono::seconds(1));
            } catch (const FiberCanceledException&) {
                done++;
            }
        });

        g.Spawn([&] {
            SleepFor(std::chrono::milliseconds(1));
            done++;
        });

        SleepFor(std::chrono::milliseconds(10));
    }

    REQUIRE(done == 2);
}
