#include "reader.h"

namespace cactus {

void IReader::ReadFull(MutableView buf) {
    size_t len = 0;
    while (buf.size() != 0) {
        size_t n = Read(buf);
        if (n == 0) {
            break;
        }

        buf += n;
        len += n;
    }

    if (buf.size() != 0) {
        throw EOFException{};
    }
}

std::string IReader::ReadAllToString() {
    std::string out(1024, '\0');
    size_t pos = 0;

    while (true) {
        if (pos == out.size()) {
            out.resize(out.size() * 2);
        }

        auto buf = View(out);
        buf += pos;

        auto len = Read(buf);
        if (len == 0) {
            break;
        }

        pos += len;
    }

    out.resize(pos);
    return out;
}

size_t IBufferedReader::Read(MutableView buf) {
    size_t bytes_read = 0;

    while (true) {
        auto chunk = ReadNext();
        if (chunk.size() == 0) {
            return bytes_read;
        }

        size_t copy_size = Copy(buf, chunk);
        buf += copy_size;
        chunk += copy_size;
        bytes_read += copy_size;

        if (buf.size() == 0) {
            if (chunk.size() != 0) {
                ReadBackUp(chunk.size());
            }

            return bytes_read;
        }
    }
}

BufferReader::BufferReader(ConstView buf) : buf_(buf) {
}

size_t BufferReader::Read(MutableView buf) {
    auto written = Copy(buf, buf_);
    buf_ += written;
    return written;
}

}  // namespace cactus