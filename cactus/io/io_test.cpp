#include <catch2/catch.hpp>

#include <cactus/io/writer.h>

using namespace cactus;

TEST_CASE("Formatting") {
    StringWriter w;

    w.Format("{} {} {}", 1, 1.25, "abc");
    REQUIRE("1 1.25 abc" == w.String());
}