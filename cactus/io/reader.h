#pragma once

#include "view.h"

#include <exception>

namespace cactus {

class EOFException : public std::exception {
public:
    EOFException() = default;

    const char* what() const noexcept override {
        return "error: EOF";
    }
};

class IReader {
public:
    virtual ~IReader() = default;

    virtual size_t Read(MutableView buf) = 0;

    void ReadFull(MutableView buf);

    std::string ReadAllToString();
};

class IBufferedReader : public IReader {
public:
    virtual MutableView ReadNext() = 0;
    virtual void ReadBackUp(size_t size) = 0;

    // Default implementation.
    virtual size_t Read(MutableView buf) override;
};

class BufferReader : public IReader {
public:
    BufferReader(ConstView buf);

    virtual size_t Read(MutableView buf) override;

private:
    ConstView buf_;
};

}  // namespace cactus