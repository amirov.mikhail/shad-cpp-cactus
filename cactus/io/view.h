#pragma once

#include <folly/io/IOBuf.h>

#include <boost/asio/buffer.hpp>

namespace cactus {

/**
 * MutableView - задаёт невладеющую ссылку на непрерывный участок памяти.
 * Буфер задаётся адресом начала участка памяти и размером в байтах.
 *
 *     MutableView buf;
 *     void* data = buf.data();
 *     size_t size = buf.size();
 *
 * Для MutableView определён operator += (int n), сдвигающий начало буфера вперёд.
 * Удобно использовать его в циклах, когда нужно одновременно сдвинуть начало буфера
 * вперёд и уменьшить его размер.
 *
 *     void ReadAll(MutableView buf) {
 *         while (buf.size() != 0) {
 *             size_t n = Read(buf);
 *             buf += n;
 *         }
 *     }
 */
using MutableView = boost::asio::mutable_buffer;

/**
 * TConstView - аналог MutableView, запрещающий менять память, на которую ссылается.
 * Используется в случае, когда нужно передать участок памяти внутрь функции
 * только для чтения, например:
 *
 *     void Write(TConstView buf);
 *
 * Неявно конструируется из MutableView.
 */
using ConstView = boost::asio::const_buffer;

/**
 * View - создаёт буфер, ссылающийся на объект.
 *
 *     int i;
 *     auto buf = View(i); // Создаёт MutableView ссылающийся на 4 байта переменной i.
 *
 *     std::vector<char> data(1024);
 *     auto buf = View(data); // Создаёт буфер ссылающийся на 1024 байта массива data.
 *
 *     struct Header {
 *         int ip;
 *         char flag;
 *     } __attribute__((packed)) header;
 *     auto buf = View(header);
 */
template <class T>
MutableView View(T& pod) {
    return boost::asio::buffer(pod);
}

inline ConstView View(std::string_view sv) {
    return boost::asio::buffer(sv);
}

/**
 * View перегружен для const ссылок.
 *
 *     const int i = 42;
 *     auto buf = View(i); // buf имеет тип ConstView.
 */
template <class T>
ConstView View(const T& pod) {
    return boost::asio::buffer(pod);
}

/**
 * View можно создать из пары (указатель, длина). Обычно используются указатель типа void или
 * char.
 */
template <class Ptr>
inline MutableView View(Ptr* data, size_t size) {
    return boost::asio::buffer(data, size);
}

/**
 * View создаст ConstView, если ему передать указатель на const.
 */
template <class Ptr>
inline ConstView View(const Ptr* data, size_t size) {
    return boost::asio::buffer(data, size);
}

/**
 * Copy копирует содержимое in в out.
 *
 * Функция возвращает количество скопированых байт, равное минимуму из in.size() и out.size().
 */
inline size_t Copy(MutableView out, ConstView in) {
    return boost::asio::buffer_copy(out, in);
}

}  // namespace cactus