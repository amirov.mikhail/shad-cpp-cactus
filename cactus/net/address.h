#pragma once

#include <optional>
#include <variant>
#include <array>
#include <string>
#include <cstdint>

#include <folly/SocketAddress.h>

namespace cactus {

class IResolver {
public:
    virtual ~IResolver() = default;

    virtual folly::SocketAddress Resolve(std::string_view address) = 0;
};

}  // namespace cactus
