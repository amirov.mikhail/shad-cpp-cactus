* Core
** DONE View type
** Docs
** Exceptions
** Random scheduling
** Fault injection
** Mutex
** RWMutex
** Semaphore
** Select
* Network
** DONE TCP Socket
** DNS
** UDP Socket
** ICMP Socket
** UNIX Socket
** Socket Options
** TLS Socket
* Protocols
** HTTP
** Redis
* Contrib
** snappy
** DONE fmtlib
** DONE protobuf
** DONE glog
** DONE gflags
* Examples
** DONE Daytime protocol
** Echo protocol
* CI
** clang-format
** clang-tidy
** ubuntu build
** sanitizers
