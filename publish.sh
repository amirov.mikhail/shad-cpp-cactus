#!/bin/bash

set -x

git checkout master -- \
    cactus \
    cmake \
    README.md \
    CMakeLists.txt \
    portknock \
    portscan \
    socks4 \
    fanout

tee {portknock,portscan,socks4,fanout}/main.cpp <<EOF
int main(int argc, char* argv[]) {
    return 0;
}
EOF

tee < /dev/null \
    portknock/portknock.cpp \
    portscan/portscan.cpp \
    socks4/socks4.cpp

git add .
